# File service TZ
Данное решение не предусматривает HTTP запросов, не разварачивает никакую инфраструктуру,
а просто имитирует работу с серверами для того, чтобы показать образ мышления разработчека

# Как все работает
### Структура проекта
1. Папка `/resouces` содержит один единственный файлик, который будет имитировать загруженный файл (PUT) 
2. Папка `/servers` будет содержать в себе подпапки созданные программно, которые будут представлять собой сервера для хранения частей файла
Example:
```
/services -|
    - service_1 -|   
    - service_2 -|   
    - ...       -|   
    - service_n -|
```
  
### Запуск:
Заходим в корен проекта и выполняем команду
``go run ./cmd -servers=5``, 
где ``-servers`` параметр, который определяет сколько в системе серверов

### Результат
В результате выполнения кода в подпапках - серверах появятся части файла, 
из которых в корне проекта будет создан файл с именем `image01_restored.jpg`

## P.S. 
Изначально я развернул проект в докере в который входил сервер для загрузки файлов, а так же 6 серверов для хранения файлов.
Выполнялся автодисковеринг серверов для хранения файлов и потом по ним, используя похожую логику, раскидывались части файлика, 
но после разговора с HR я понял, что всех больше интересовал сам подход к решению, нежели организация когда, 
поэтому было принято решение просто реализовать в одном файле само решение и отбросить остальное)) Так что если что не серчайте, 
я могу представить и другой вариант по необходимости.

