package main

import (
	"bytes"
	"flag"
	"fmt"
	"log"
	"math/rand"
	"os"
	"path"
	"strconv"
	"strings"
	"time"
)

const (
	servicesBasePath = "./services"
	fileName         = "image01.jpg"
	srcFilePath      = "./resources/"
)

func main() {
	fmt.Println("=======================================")
	fmt.Println("PUT file image01.jpg")
	fmt.Println("=======================================")

	var serversCount int
	flag.IntVar(&serversCount, "servers", 5, "amount of up and running servers")
	flag.Parse()

	CleanUpFolder(servicesBasePath)

	var serviceFolderPath string
	services := make([]string, 0, serversCount)
	shuffServices := make([]string, 0, serversCount)
	for i := 1; i <= serversCount; i++ {
		serviceFolderPath = strings.Join([]string{servicesBasePath, fmt.Sprintf("service_%d", i)}, "/")
		services = append(services, serviceFolderPath)
		shuffServices = append(shuffServices, serviceFolderPath)
	}
	// Just to be sure that we don't care about servers sequencing
	ShuffleSlice(shuffServices)
	fmt.Println("Service list: ", services)

	file, err := os.Stat(srcFilePath + fileName)
	if err != nil {
		log.Fatal(err)
	}

	partSize := file.Size() / int64(serversCount)
	data, err := os.ReadFile(srcFilePath + fileName)
	if err != nil {
		log.Fatal(err)
	}

	for i, service := range shuffServices {
		start := int64(i) * partSize
		end := start + partSize
		if i == serversCount-1 {
			end = file.Size()
		}
		part := data[start:end]
		_, err = os.Stat(service)
		if os.IsNotExist(err) {
			ServiceStart(service)
		}
		// Prepend bytes with metadata
		// сейчас это просто один байт означающий порядковый номер части файла, но в реальном мире, возможно, метадата была бы больше,
		// чем просто uint8 значение, тогда пришлось бы первым байтом поставить значение, которые определяет длину метадаты,
		// чтобы потом можно было безошибочно определить с какого байта начинается тело самого документа
		metadataByte := []byte(strconv.Itoa(i + 1))
		metadataByte = append([]byte{metadataByte[0]}, part...)
		partFilePath := fmt.Sprintf("%s/%s", service, fileName+".part")
		err = os.WriteFile(partFilePath, metadataByte, 0644)
		if err != nil {
			log.Fatal(err)
		}
		fmt.Printf("Part of the file has been created: %s\n\n", partFilePath)
	}
	fmt.Println("=======================================")
	fmt.Println("GET file image01.jpg")
	fmt.Println("=======================================")

	partsMap := make(map[int][]byte, 0)
	for _, service := range services {
		inputData, err := os.ReadFile(service + "/" + fileName + ".part")
		if err != nil {
			log.Fatal("Failed to open file", err)
		}
		fileOrder := ExtractOrderMetadata(inputData)
		partsMap[fileOrder] = inputData[1:]
	}
	resultFileData := make([]byte, 0)
	for i := 1; i <= serversCount; i++ {
		bits, ok := partsMap[i]
		if !ok {
			continue
		}
		resultFileData = append(resultFileData, bits...)
	}

	err = os.WriteFile("./image01_restored.jpg", resultFileData, 0644)
	if err != nil {
		log.Fatal(err)
	}

	fmt.Println("=================================")
	fmt.Printf("Compare bytes: %v\n", bytes.Equal(data, resultFileData))
	fmt.Println("=================================")
	fmt.Println("check for `image01_restored.jpg` in the project root directory")
	os.Exit(0)
}

func ServiceStart(subFolderPath string) {
	err := os.Mkdir(subFolderPath, 0777)
	if err != nil {
		log.Fatal("cannot start service:", err)
	}
	fmt.Println("Service started")
}

func ShuffleSlice(s []string) {
	rand.Seed(time.Now().UnixNano())
	rand.Shuffle(len(s), func(i, j int) { s[i], s[j] = s[j], s[i] })
}

func CleanUpFolder(directory string) {
	dir, _ := os.ReadDir(directory)
	for _, d := range dir {
		_ = os.RemoveAll(path.Join([]string{directory, d.Name()}...))
	}
}

func ExtractOrderMetadata(data []byte) int {
	orderByte := string(data[0])
	order, _ := strconv.Atoi(orderByte)

	return order
}
